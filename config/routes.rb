Rails.application.routes.draw do
  root 'blog_scraping#home'
  post '/show' => 'blog_scraping#show'
  get '*path', controller: 'application', action: 'render_404'
end
