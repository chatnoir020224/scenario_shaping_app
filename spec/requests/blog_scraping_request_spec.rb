require 'rails_helper'

RSpec.describe "BlogScrapings", type: :request do

  describe "GET /home" do
    it "returns http success" do
      get "/blog_scraping/home"
      expect(response).to have_http_status(:success)
    end
  end

end
