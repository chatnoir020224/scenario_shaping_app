require 'rails_helper'

RSpec.describe 'FormatIntoScenarioController', type: :routing do
  describe 'routing' do
    it 'routes to #home' do
      expect(get: "/format_into_scenario/home").to route_to("format_into_scenario#home")
    end
  end
end
