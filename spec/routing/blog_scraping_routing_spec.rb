require 'rails_helper'

RSpec.describe 'BlogScrapingController', type: :routing do
  describe 'routing' do
    it 'routes to #home' do
      expect(get: "/blog_scraping/home").to route_to("blog_scraping#home")
    end
  end
end
