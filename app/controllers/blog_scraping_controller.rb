class BlogScrapingController < ApplicationController
  protect_from_forgery
  include BlogScraping
  def home
  end

  def show
    # PostパラメータからURL取得
    urls = params[:urls].split("\r\n")
    # TODO model不要のバリデーション調査する
    # if urls.blank?
    #   flash[:alert] = I18n.t('scraping.no_input_url')
    #   redirect_to root_path
    # end

    agent = Mechanize.new
    @blog_content = multiple_scraping(agent, urls)
  end
end
