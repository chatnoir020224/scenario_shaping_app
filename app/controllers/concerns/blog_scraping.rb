module BlogScraping
  extend ActiveSupport::Concern

  # 複数URLのスクレイピング
  def multiple_scraping(agent, urls)
    sentences = Array.new
    urls.each{|u| sentences.push(scraping(agent, u))}
    raise if sentences.blank?

    sentences.join("\n")
  end
  
  # 1URLのスクレイピング
  def scraping(agent, url)
    begin
      page = agent.get(url)
      element = page.search('p')
      sentence = Array.new
      element.each{|e| sentence.push(e.inner_text)}

      sentence.join("\n")
    rescue Exception => e
      raise
    end
  end
end