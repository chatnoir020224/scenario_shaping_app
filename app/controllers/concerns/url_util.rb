module UrlUtil
  extend ActiveSupport::Concern
  def create_urls(agent)
    # 作者専用URL作成ツールなので、全く汎用的ではありません。飽くまで準備ツールです。
    # 今後、必要がある場合は、はてなブログ専用にして、はてなブログのAPIでURLを取得する予定です。
    access_urls = Array.new
    error_urls = Array.new
    
    # URLチェック+URL作成
    (Date.parse('2020/04/26')..Date.parse('2020/06/24')).each{|date| checkUrl(create_url(date), agent) ? access_urls.push(create_url(date)) : error_urls.push(create_url(date))}

    access_urls
  end

  # URL作成
  def create_url(date)
    Settings.hatena[:base_url] + date.strftime("%Y/%m/%d") + Settings.hatena[:url_fixed_part]
  end

  # URLチェック
  def checkUrl(url, agent)
    begin
      agent.get(url)
      true
    rescue => e
      puts e
      false
    end
  end
end