class ApplicationController < ActionController::Base
  rescue_from ActionController::RoutingError, with: :render_404
  rescue_from Exception, with: :render_404

  def render_404
    render template: '404.html', status: 404, layout: 'application', content_type: 'text/html'
  end
end
